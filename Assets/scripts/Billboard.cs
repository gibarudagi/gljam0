﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;

namespace tg
{
	[ExecuteAlways]
	public class Billboard : MonoBehaviour
	{
		public Transform root;
		public bool      only_y;
		#region private
		#endregion

		#region Unity
		private void Awake()
		{
			if ( root == null )
			{
				root = transform;
			}
		}
		private void Start()
		{
		}
		private void Update()
		{
		}
		private void OnWillRenderObject()
		{
			if ( ! gameObject.activeInHierarchy || ! enabled )
			{
				return;
			}
			if ( only_y )
			{
				Vector3 p = Camera.current.transform.position;
				p.y = transform.position.y;
				root.LookAt( p );
			} else
			{
				root.LookAt( Camera.current.transform );
			}
		}
		#endregion
	}
}
