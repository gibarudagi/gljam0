﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Status : MonoBehaviour
{
	private float m_currentHealth; //percentage. from 0 to 1
	private float m_stamina; //from 0 to 1

	public UnityAction on_die;
	public UnityAction<float, GameObject> on_damage;//passes amount of damage and source
	public UnityAction<float> on_heal;

	public bool isDead;
	public float current_health
	{
		get => m_currentHealth;
		set { m_currentHealth = value; }
	}

	public float max_health;
	public float stamina
	{
		get => m_stamina;
	}

	public void heal(float amount)
	{
		if (amount <= 0) return;

		current_health += amount / max_health;
		on_heal?.Invoke(amount);
	}

	public void damage(float amount, GameObject source)
	{
		if (isDead) return;

		current_health -= amount / max_health;

		on_damage?.Invoke(amount, source);

		if (current_health <= 0)
		{
			isDead = true;
			on_die?.Invoke();
		}
	}
}
