﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;
using static tg.Character_Controller;

namespace tg
{
	public class MOB_Commander : MonoBehaviour
	{
		public Character_Controller character;

		[Header("Parameters")]
		public float angle_eps_deg = 0.1f;
		public float distance_eps = 0.1f;
		public float max_rotation_deg = 20f;
		public float dist_run_fwd = 4f;
		public float dist_run_side = 2f;
		public float ray_max_distance = 1f;
		public float entity_radius = 0.5f;

		[Header("Targets")]
		//public float  target_distance = 0f;
		public float  target_distance_range_min = 0f;
		public float  target_distance_range_max = 0f;
		public float3 goto_target;
		public float3 look_target;
		public Entity entity_target;

		public bool debug_target_active = false;
		public Transform debug_target;
		public Transform debug_look_target_ghost;
		public Transform debug_hit_point;

		// TODO(theGiallo): add bool has_*_target?

		public bool is_looking_at_target { get; private set; }
		public bool is_at_target { get; private set; }

		public
		void
		attack()
		{
			commands.actions_just_on[(int)Action_Index.USE] = 1;
		}
		public
		void
		cast()
		{
			commands.actions_just_on[(int)Action_Index.USE_ALT] = 1;
		}

		#region private
		private Character_Controller.Controls_Commands commands = new Controls_Commands();
		private int look_layer_mask;

		private
		bool
		rotate_towards( float3 pos, out float angle_diff_deg )
		{
			bool ret = false;
			Transform tr = character.body_tr;
			float3 fw = tr.forward;
			float3 fw_target = pos - float3(tr.position);
			float desired_deg = Vector3.SignedAngle( fw, fw_target, Vector3.up );
			angle_diff_deg = desired_deg;

			if ( abs( angle_diff_deg ) >= angle_eps_deg )
			{
				commands.delta_yaw_deg = clamp( desired_deg, -max_rotation_deg, max_rotation_deg );
				ret = true;
			}
			return ret;
		}
		private
		void
		stop_walking()
		{
			ensure_off( Action_Index.RUN );
			ensure_off( Action_Index.WALK );
			ensure_off( Action_Index.WALK_BACK );
			ensure_off( Action_Index.WALK_SIDEWAYS_LEFT );
			ensure_off( Action_Index.WALK_SIDEWAYS_RIGHT );
		}
		private
		void
		ensure_off( Action_Index action )
		{
			if ( commands.actions_on[(int)action] == 1 )
			{
				commands.actions_just_off[(int)action] = 1;
			}
		}
		#endregion

		#region Unity
		private void Awake()
		{
			look_layer_mask = ~LayerMask.GetMask( "step_checker" );
		}
		private void Start()
		{
		}
		private void Update()
		{
			if ( debug_target_active )
			{
				look_target = goto_target = debug_target.position;
			}

			float3 pos = character.transform.position;
			pos.y += entity_radius;
			float3 target_vec = goto_target - pos;
			target_vec.y = pos.y;
			float dist = length( target_vec );

			float angle_diff_deg;

			if ( dist > 0.01f )
			{
				float3 desired_dir = target_vec / dist;
				//desired_dir = character.transform.forward;
				bool did_hit = false;
				RaycastHit[] hits = initialized_array<RaycastHit>( 8 );

				float3[] rays_origins = new float3[]{
				 pos + entity_radius * float3(  desired_dir.z, pos.y, -desired_dir.x ),
				 pos + entity_radius * float3( -desired_dir.z, pos.y,  desired_dir.x ),
				 pos + entity_radius * desired_dir
				};
				Ray ray = new Ray();
				ray.direction = desired_dir;
				RaycastHit hit = new RaycastHit();
				float min_dist = float.MaxValue;
				for ( int ri = 0; ri != rays_origins.Length; ++ri )
				{
					ray.origin = rays_origins[ri];
					int hits_count = Physics.RaycastNonAlloc( ray, hits, ray_max_distance );

					for ( int i = 0; i != hits.Length && i < hits_count; ++i )
					{
						if ( hits[i].transform.IsChildOf( character.transform )
						  || ( entity_target != null
						    && hits[i].transform.IsChildOf( entity_target.transform ) )
						)
						{
							continue;
						}
						if ( hits[i].distance < min_dist )
						{
							hit = hits[i];
							min_dist = hits[i].distance;
							did_hit = true;
						}
					}
				}
				if ( did_hit )
				{
					float2 desired_dir2 = float2( desired_dir.x, desired_dir.z );
					float2 normal2 = float2( hit.normal.x, hit.normal.z );
					float angle_deg = Vector2.SignedAngle( desired_dir2, -normal2 );
					float a = 90 * sign( angle_deg );
					float3 lp = pos + float3( Quaternion.AngleAxis( a, Vector3.up ) * desired_dir );

					float2 d = desired_dir2 - normal2 * dot( desired_dir2, normal2 );
					lp = pos + float3( d.x, 0, d.y ) / length( d ) * 2f * entity_radius;
					//log( $"lp = {lp} {hit.collider.gameObject.name}" );
					if ( debug_look_target_ghost != null ) debug_look_target_ghost.position = lp;
					if ( debug_hit_point != null ) debug_hit_point.position = hit.point;

					is_looking_at_target = !rotate_towards( lp, out angle_diff_deg );
					//log( $"angle deg {angle_deg} diff {angle_diff_deg}" );
					angle_diff_deg = 0;
				} else
				{
					is_looking_at_target = !rotate_towards( look_target, out angle_diff_deg );
				}
			} else
			{
				is_looking_at_target = !rotate_towards( look_target, out angle_diff_deg );
			}


			is_at_target = false;
			//log( $"angle {angle_diff_deg} dist {dist} desired_distance + distance_eps {desired_distance + distance_eps}" );
			if ( dist > target_distance_range_max + distance_eps )
			{
				if ( angle_diff_deg >= -45 && angle_diff_deg <= 45 )
				{
					commands.actions_just_on[(int)Action_Index.WALK] = 1;
					ensure_off( Action_Index.WALK_BACK );
					ensure_off( Action_Index.WALK_SIDEWAYS_LEFT );
					ensure_off( Action_Index.WALK_SIDEWAYS_RIGHT );
					if ( dist > dist_run_fwd )
					{
						commands.actions_just_on[(int)Action_Index.RUN] = 1;
					} else
					{
						ensure_off( Action_Index.RUN );
					}
				} else
				if ( angle_diff_deg < -45 )
				{
					commands.actions_just_on[(int)Action_Index.WALK_SIDEWAYS_LEFT] = 1;
					ensure_off( Action_Index.WALK_BACK );
					ensure_off( Action_Index.WALK );
					ensure_off( Action_Index.WALK_SIDEWAYS_RIGHT );
					if ( dist > dist_run_side )
					{
						commands.actions_just_on[(int)Action_Index.RUN] = 1;
					} else
					{
						ensure_off( Action_Index.RUN );
					}
				} else
				if ( angle_diff_deg > 45 )
				{
					commands.actions_just_on[(int)Action_Index.WALK_SIDEWAYS_RIGHT] = 1;
					ensure_off( Action_Index.WALK_BACK );
					ensure_off( Action_Index.WALK_SIDEWAYS_LEFT );
					ensure_off( Action_Index.WALK );
					if ( dist > dist_run_side )
					{
						commands.actions_just_on[(int)Action_Index.RUN] = 1;
					} else
					{
						ensure_off( Action_Index.RUN );
					}
				} else
				{
					stop_walking();
				}
			} else
			if ( dist < target_distance_range_min - distance_eps )
			{
				if ( angle_diff_deg >= -45 && angle_diff_deg <= 45 )
				{
					commands.actions_just_on[(int)Action_Index.WALK_BACK] = 1;
					ensure_off( Action_Index.WALK );
					ensure_off( Action_Index.WALK_SIDEWAYS_LEFT );
					ensure_off( Action_Index.WALK_SIDEWAYS_RIGHT );
				} else
				if ( angle_diff_deg < -45 )
				{
					commands.actions_just_on[(int)Action_Index.WALK_SIDEWAYS_RIGHT] = 1;
					ensure_off( Action_Index.WALK_BACK );
					ensure_off( Action_Index.WALK );
					ensure_off( Action_Index.WALK_SIDEWAYS_LEFT );
				} else
				if ( angle_diff_deg > 45 )
				{
					commands.actions_just_on[(int)Action_Index.WALK_SIDEWAYS_LEFT] = 1;
					ensure_off( Action_Index.WALK_BACK );
					ensure_off( Action_Index.WALK_SIDEWAYS_RIGHT );
					ensure_off( Action_Index.WALK );
				} else
				{
					commands.actions_just_on[(int)Action_Index.WALK] = 1;
					ensure_off( Action_Index.WALK_BACK );
					ensure_off( Action_Index.WALK_SIDEWAYS_LEFT );
					ensure_off( Action_Index.WALK_SIDEWAYS_RIGHT );
				}
			} else
			{
				stop_walking();
				is_at_target = true;
			}

			commands.actions_on |= commands.actions_just_on;
			commands.actions_on &= ~commands.actions_just_off;
		}
		private void FixedUpdate()
		{
			character.controls_commands.actions_on      |= commands.actions_just_on;
			character.controls_commands.actions_on      &= ~commands.actions_just_off;
			character.controls_commands.actions_just_on  .copy_from( ref commands.actions_just_on );
			character.controls_commands.actions_just_off .copy_from( ref commands.actions_just_off );
			character.controls_commands.delta_pitch_deg  = commands.delta_pitch_deg;
			character.controls_commands.delta_yaw_deg    = commands.delta_yaw_deg;

			//log( $"walk fw = {character.controls_commands.actions_on[(int)Action_Index.WALK] == 1}"
			//   + $" walk bk = {character.controls_commands.actions_on[(int)Action_Index.WALK_BACK] == 1}"
			//   + $" walk l  = {character.controls_commands.actions_on[(int)Action_Index.WALK_SIDEWAYS_LEFT] == 1}"
			//   + $" walk r  = {character.controls_commands.actions_on[(int)Action_Index.WALK_SIDEWAYS_RIGHT] == 1}"
			//);
			//log( $"COMMAND walk fw = {commands.actions_on[(int)Action_Index.WALK] == 1}"
			//   + $" walk bk = {commands.actions_on[(int)Action_Index.WALK_BACK] == 1}"
			//   + $" walk l  = {commands.actions_on[(int)Action_Index.WALK_SIDEWAYS_LEFT] == 1}"
			//   + $" walk r  = {commands.actions_on[(int)Action_Index.WALK_SIDEWAYS_RIGHT] == 1}"
			//);

			commands.actions_just_on .clear();
			commands.actions_just_off.clear();
			commands.delta_pitch_deg = 0;
			commands.delta_yaw_deg   = 0;
		}
		#endregion
	}
}
