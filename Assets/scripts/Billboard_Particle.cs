﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;

namespace tg
{
	public abstract class Billboard_Particle : MonoBehaviour, IPooled<Billboard_Particle>
	{
		virtual protected GameObject the_prefab { get; set; }
		virtual protected Pool<Billboard_Particle> the_pool { get; set; }

		public Tile_Map_Sprite tile_map_sprite;

		#region pool
		protected int index;
		public
		int
		get_pool_index()
		{
			return index;
		}

		public
		Billboard_Particle
		new_one()
		{
			Billboard_Particle ret;
			GameObject go = Instantiate( the_prefab );
			ret = go.GetComponent<Billboard_Particle>();
			return ret;
		}

		public
		void
		put_back_in_pool()
		{
			the_pool.put_back( this );
			gameObject.SetActive( false );
			transform.SetParent( the_prefab.transform.parent );
		}

		public
		void
		set_pool_index( int new_pool_index )
		{
			index = new_pool_index;
		}
		#endregion pool

		#region private
		#endregion

		//#region Unity
		//private void Awake()
		//{
		//}
		//private void Start()
		//{
		//}
		//private void Update()
		//{
		//}
		//#endregion
	}
}
