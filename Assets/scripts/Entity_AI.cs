﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;
using System;

namespace tg
{
	public class Entity_AI : MonoBehaviour
	{
		public const ushort HP_CHECK_PRIORITY = 0;
		public const ushort ALIVE_CHECK_PRIORITY = 0;
		public const ushort WEAPON_CHANGE_PRIORITY = 0;

		[Serializable]
		public class
		Initial_Hostility_Per_Distance
		{
			public Entity.Type type;
			public Hostility_Per_Distance hostility_per_distance;
		}
		[Serializable]
		public class
		Hostility_Per_Distance
		{
			public const int BUCKETS_COUNT = 8;
			public float hostility_update_alpha = 0.01f;
			public float[] hostilities = new float[BUCKETS_COUNT];
			readonly public static float2[] ranges = new float2[BUCKETS_COUNT];
			static Hostility_Per_Distance()
			{
				for ( int i = 0; i != BUCKETS_COUNT; ++i )
				{
					ranges[i].x = pow( 2f, (float)i ) - 1f;
					ranges[i].y = pow( 2f, (float)i + 1f) - 1f;
				}
			}

			public static
			int
			hostility_index( float distance )
			{
				int ret;
				distance = max( 0, distance );
				ret = (int)floor( log2( distance + 1 ) );
				ret = min( ret, BUCKETS_COUNT - 1 );
				return ret;
			}
			public static
			void
			zone_range( int index, out float min_dist, out float max_dist )
			{
				debug_assert( index >= 0 );
				debug_assert( index < BUCKETS_COUNT );

				min_dist = ranges[index].x;
				max_dist = ranges[index].y;
			}
			public
			float
			hostility( float distance )
			{
				int index = hostility_index( distance );
				float ret = hostilities[index];
				return ret;
			}
			public
			int
			update( float distance, float delta_hp )
			{
				int index = hostility_index( distance );
				// NOTE(theGiallo): with change of sign the resulting change of sign could be too fast
				hostilities[index] = lerp( hostilities[index], delta_hp, hostility_update_alpha );
				return index;
			}
			public
			void
			update( Hostility_Per_Distance hpd )
			{
				for ( int i = 0; i != hostilities.Length; ++i )
				{
					hostilities[i] = lerp( hostilities[i], hpd.hostilities[i], hostility_update_alpha );
				}
			}
			public
			void
			update( Hostility_Per_Distance hpd, Bit_Vector mask )
			{
				for ( int i = 0; i != hostilities.Length; ++i )
				{
					if ( mask[i] == 1 )
					{
						hostilities[i] = lerp( hostilities[i], hpd.hostilities[i], hostility_update_alpha );
					}
				}
			}

			public
			bool
			nearest_safe_zone( float current_distance, out int index, Func<float,bool> hostility_is_friend )
			{
				bool ret = false;
				index = -1;

				int this_index = hostility_index( current_distance );
				zone_range( this_index, out float min_dist, out float max_dist );

				return ret;
			}
			public
			float
			avg()
			{
				float ret = 0;
				for ( int i = 0; i != hostilities.Length; ++i )
				{
					ret += hostilities[i];
				}
				ret /= (float) hostilities.Length;
				return ret;
			}

			public void clear()
			{
				for ( int i = 0; i != hostilities.Length; ++i )
				{
					hostilities[i] = 0;
				}
			}

			public
			void
			copy_from( Hostility_Per_Distance hostility_per_distance )
			{
				hostility_update_alpha = hostility_per_distance.hostility_update_alpha;
				for ( int i = 0; i != hostilities.Length; ++i )
				{
					hostilities[i] = hostility_per_distance.hostilities[i];
				}
			}
		}
		public class
		Knowledge_Base
		{
			public class
			Known_Entity
			{
				public Entity entity;
				public int    uuid;
				public float3 position;
				public float  radius;
				public float  hp;
				public float  max_hp;
				public float  dps;
				public bool   is_alive;

				public float  time_secs;

				public Known_Entity( Entity e )
				{
					entity   = e;
					uuid     = e.uuid;
					update();
				}
				public void update()
				{
					position  = entity.transform.position;
					radius    = entity.radius;
					hp        = entity.status.hp;
					max_hp    = entity.status.max_hp;
					is_alive  = entity.status.is_alive;
					dps       = entity.status.dps;
					time_secs = Time.time;
				}

				public
				float
				age_secs()
				{
					float ret = Time.time - time_secs;
					return ret;
				}
			}
			public
			Knowledge_Base()
			{
				for ( int i = 0; i != hostilities_per_dist_per_type.Length; ++i )
				{
					hostilities_per_dist_per_type         [i] = new Hostility_Per_Distance();
					hostilities_per_dist_per_type_timestep[i] = new Hostility_Per_Distance();
					hostilities_per_dist_per_type_timestep_did_update[i] = new Bit_Vector( Hostility_Per_Distance.BUCKETS_COUNT );
					known_entities_per_type               [i] = new List<Known_Entity>();
				}
			}

			public
			void
			update_hostility_immediately( Entity.Type type, float distance, float delta_hp )
			{
				Hostility_Per_Distance hpd = hostilities_per_dist_per_type[(int)type];
				hpd.update( distance, delta_hp );
				float avg = hpd.avg();
				hostilities_per_type[(int)type] = avg;
			}
			public
			void
			update_hostility_timestep( Entity.Type type, float distance, float delta_hp )
			{
				Hostility_Per_Distance hpd = hostilities_per_dist_per_type_timestep[(int)type];
				int i = hpd.update( distance, delta_hp );
				hostilities_per_dist_per_type_timestep_did_update[(int)type][i] = 1;
			}
			public
			void
			resolve_hostility_timestep( float news_limit_secs )
			{
				float now_secs = Time.time;
				float time_limit_secs = now_secs - news_limit_secs;

				for ( int ti = 0; ti != hostilities_per_dist_per_type.Length; ++ti )
				{
					Hostility_Per_Distance hpd          = hostilities_per_dist_per_type         [ti];
					Hostility_Per_Distance hpd_timestep = hostilities_per_dist_per_type_timestep[ti];
					ref Bit_Vector bv = ref hostilities_per_dist_per_type_timestep_did_update[ti];
					for ( int di = 0; di != hostilities_per_dist_per_type_timestep_did_update[ti].bits_count; ++di )
					{
						if ( bv[di] == 0 )
						{
							for ( int ei = 0; ei != known_entities_per_type[ti].Count; ++ei )
							{
								Known_Entity ke = known_entities_per_type[ti][ei];
								if ( ke.time_secs >= time_limit_secs )
								{
									int edi = Hostility_Per_Distance.hostility_index( entity.distance( ke.position, ke.radius ) );
									if ( edi == di )
									{
										bv[di] = 1;
										break;
									}
								}
							}
						}
					}
					hpd.update( hpd_timestep, bv );
					hpd_timestep.clear();
					bv.clear();
					float avg = hpd.avg();
					hostilities_per_type[ti] = avg;
				}
			}

			public
			void
			know( Entity e )
			{
				Known_Entity ke;
				Entity.Type type = e.type;
				int index = known_entities_per_type[(int)type].FindIndex( ( le ) => le.uuid == e.uuid );
				if ( index < 0 )
				{
					index = known_entities_per_type[(int)type].Count;
					ke = new Known_Entity( e );
					known_entities.Add( ke );
					known_entities_per_type[(int)type].Add( ke );
				} else
				{
					ke = known_entities_per_type[(int)type][index];
					ke.update();
				}
			}

			public
			bool
			friend_in_need_of_heealing( out Known_Entity friend, float news_limit_secs )
			{
				bool ret = false;
				friend = null;

				float now_secs = Time.time;
				float time_limit_secs = now_secs - news_limit_secs;

				/* TODO(theGiallo): we serve the friend most in need, regardless of distance or friendliness
				   Maybe we should weight on distance and friendliness?
				 */
				float max_need = 0;
				for ( int it = 0; it != hostilities_per_type.Length; ++it )
				{
					float hostility = hostilities_per_type[it];
					if ( hostility_is_friend( hostility ) )
					{
						for ( int ie = 0; ie != known_entities_per_type[it].Count; ++ie )
						{
							Known_Entity ke = known_entities_per_type[it][ie];
							if ( ke.time_secs < time_limit_secs )
							{
								continue;
							}

							float need = 1f - ke.hp / ke.max_hp;
							if ( need > 0 && need > max_need )
							{
								friend = ke;
								ret = true;
							}
						}
					}
				}

				return ret;
			}

			public enum
			Which_Hostility
			{
				GENERAL,
				AT_THIS_DISTANCE
			}
			public
			bool
			most_hostile( out Known_Entity           hostile,
			              out float                  hostile_distance,
			              out Hostility_Per_Distance hostility_per_distance,
			              out int                    hostility_index,
			              float           news_limit_secs,
			              Which_Hostility which_hostility
			)
			{
				bool ret = false;
				hostile = null;
				hostile_distance = -1;
				hostility_per_distance = null;
				hostility_index = -1;

				float now_secs = Time.time;
				float time_limit_secs = now_secs - news_limit_secs;

				float max_hostility = float.MinValue;
				for ( int it = 0; it != hostilities_per_type.Length; ++it )
				{
					float hostility = hostilities_per_type[it];
					if ( which_hostility == Which_Hostility.AT_THIS_DISTANCE
					  || ! hostility_is_friend( hostility ) )
					{
						for ( int ie = 0; ie != known_entities_per_type[it].Count; ++ie )
						{
							Known_Entity ke = known_entities_per_type[it][ie];
							if ( ke.time_secs < time_limit_secs )
							{
								continue;
							}

							float distance = entity.distance( ke.position, ke.radius );
							int this_hostility_index = Hostility_Per_Distance.hostility_index( distance );
							Hostility_Per_Distance this_hostility_per_distance = hostilities_per_dist_per_type[it];
							float hostility_on_this_dist = this_hostility_per_distance.hostilities[this_hostility_index];

							if ( hostility_is_friend( hostility_on_this_dist ) )
							{
								continue;
							}

							if ( which_hostility == Which_Hostility.GENERAL )
							{
								hostility /= distance;
							} else
							if ( which_hostility == Which_Hostility.AT_THIS_DISTANCE )
							{
								hostility = hostility_on_this_dist;
							}

							if ( hostility > max_hostility )
							{
								max_hostility = hostility;
								hostile = ke;
								hostile_distance = distance;
								hostility_per_distance = this_hostility_per_distance;
								hostility_index = this_hostility_index;
								ret = true;
							}
						}
					}
				}

				return ret;
			}

			public
			bool
			hostility_is_friend( float hostility )
			{
				bool ret = hostility < 0.05f * my_max_hp;
				return ret;
			}


			public Entity entity;
			public float my_max_hp => entity.status.normal_max_hp;
			private List<Known_Entity>       known_entities                = new List<Known_Entity>();
			private List<Known_Entity>[]     known_entities_per_type       = new List<Known_Entity>[(int)Entity.Type.__COUNT];
			private Hostility_Per_Distance[] hostilities_per_dist_per_type = new Hostility_Per_Distance[(int)Entity.Type.__COUNT];
			private float[]                  hostilities_per_type          = new float[(int)Entity.Type.__COUNT];
			private Hostility_Per_Distance[] hostilities_per_dist_per_type_timestep = new Hostility_Per_Distance[(int)Entity.Type.__COUNT];
			private Bit_Vector[]             hostilities_per_dist_per_type_timestep_did_update = new Bit_Vector[ (int)Entity.Type.__COUNT ];

			public
			void
			initialize( Initial_Hostility_Per_Distance[] initial_hostilities_per_dist_per_type )
			{
				for ( int ti = 0; ti != hostilities_per_dist_per_type.Length; ++ti )
				{
					debug_assert( (int)initial_hostilities_per_dist_per_type[ti].type == ti );
					hostilities_per_dist_per_type[ti].copy_from( initial_hostilities_per_dist_per_type[ti].hostility_per_distance );
				}
			}
		}

		public Entity entity;
		public MOB_Commander commander;
		public float update_period_secs = 1f;
		public float news_limit_secs = 15f;
		public float target_entity_expiring_secs = 5f;
		public float roaming_expiring_secs = 5f;

		public float roaming_min_dist = 3;
		public float roaming_max_dist = 8;
		public float roaming_rotation_mean_degs = 40;
		public float roaming_rotation_radius    = 20;

		public Weighted_Action[] actions_weights = new Weighted_Action[3]
		{
			new Weighted_Action(){
				type = Action_Type.SURVIVE,
				weight = 0.5f
			},
			new Weighted_Action(){
				type = Action_Type.HELP,
				weight = 0.75f
			},
			new Weighted_Action(){
				type = Action_Type.KILL,
				weight = 1f
			},
		};
		public Initial_Hostility_Per_Distance[] initial_hostilities_per_dist_per_type = make_initial_hostilities_per_dist_per_type();
		[Serializable]
		public class
		Weighted_Action : IComparable<Weighted_Action>
		{
			public Action_Type type;
			public float       weight;

			public int CompareTo( Weighted_Action other )
			{
				return weight.CompareTo( other.weight );
			}
		}
		public enum
		Action_Type
		{
			SURVIVE,
			KILL,
			HELP
		}
		#region private
		private enum
		Interaction
		{
			NONE,
			HEAL,
			ATTACK,
		}
		private Knowledge_Base kb = new Knowledge_Base();

		private Interaction interaction_on_target;
		private Action_Type action_type;
		private Knowledge_Base.Known_Entity action_target_entity;
		private float target_creation_time;
		private float target_update_time;
		private float3 look_target;
		private float3 goto_target;
		private Timer  update_timer;
		private Weapon weapon;
		private int look_layer_mask;

		private
		struct
		Look_Around
		{
			public bool active { get; private set; }
			public Quaternion starting_rotation { get; private set; }

			public
			void
			activate( Quaternion current_rotation, int loops_count = 1 )
			{
				active = true;
				steps_counter = STEPS_PER_LOOP * loops_count;
				starting_rotation = this.current_rotation = current_rotation;
			}
			public
			void
			deactivate()
			{
				active = false;
			}
			public
			bool
			update( float3 pos, bool is_on_look_target, Action<float3> set_look_target )
			{
				if ( active && is_on_look_target )
				{
					if ( steps_counter == 0 )
					{
						deactivate();
					} else
					{
						--steps_counter;
						current_rotation *= Quaternion.AngleAxis( 120, Vector3.up );
						set_look_target( pos + float3( current_rotation * Vector3.forward ) );
					}
				}
				return active;
			}

			private const int STEPS_PER_LOOP = 3;
			private Quaternion current_rotation;
			private int steps_counter;
		}
		private Look_Around look_around = new Look_Around();

		private
		Chained
		my_hp_changed( Entity.StatusEntry<float>.Callback_Data data )
		{
			Chained ret = Chained.CONTINUE;
			if ( data.entity_source != null && data.entity_source.uuid != kb.entity.uuid )
			{
				// TODO(theGiallo): register these deltas per type and per distance bucket and apply them once, alltoghether, even if zero

				// NOTE(theGiallo): we got damage or healing
				Entity.Type type = data.entity_source.type;
				float hp_delta = data.previous - data.new_value;
				float distance = kb.entity.distance( data.entity_source );
				kb.update_hostility_timestep( type, distance, hp_delta );
			}
			return ret;
		}

		private
		Chained
		my_alive_changed( Entity.StatusEntry<bool>.Callback_Data data )
		{
			Chained ret = Chained.CONTINUE;
			if ( enabled != data.new_value )
			{
				enabled = data.new_value;
				if ( enabled )
				{
					become_alive();
				} else
				{
					die();
				}
			}
			return ret;
		}

		private
		Chained
		my_weapon_changed( Entity.StatusEntry<Weapon>.Callback_Data data )
		{
			Chained ret = Chained.CONTINUE;
			weapon = data.new_value;
			return ret;
		}

		private
		void
		become_alive()
		{
			commander.enabled = true;
		}
		private
		void
		die()
		{
			commander.enabled = false;
		}


		private
		void
		update_knowledge()
		{
			for ( int i = 0; i != Entity.all_entities.Count; ++i )
			{
				Entity e = Entity.all_entities[i];
				if ( e != kb.entity
				  && kb.entity.is_in_fov( e ) )
				{
					Collider c = e.entity_collider;
					float3 my_pos = kb.entity.transform.position;
					float3 e_pos = e.transform.position;
					float3 dir = e_pos - my_pos;
					// TODO(theGiallo): add jitter to dir
					Ray ray = new Ray( my_pos, dir );
					float max_distance = length( my_pos - e_pos );
					bool did_hit = Physics.Raycast( ray, out RaycastHit hit_info, max_distance, look_layer_mask );
					if ( hit_info.collider == c )
					{
						kb.know( e );
					}
					//bool did_hit = c.Raycast( ray, out RaycastHit hit_info, max_distance );
				}
			}
		}

		private
		bool
		should_fight( Knowledge_Base.Known_Entity ke )
		{
			bool ret = ke.hp / kb.entity.status.dps < kb.entity.status.hp / ke.dps;
			return ret;
		}
		private
		void
		set_look_target( float3 new_look_target )
		{
			commander.look_target = look_target = new_look_target;
		}
		private
		void
		update_goto_target( float3 new_goto_target )
		{
			commander.goto_target =
			goto_target           = new_goto_target;
		}
		private
		void
		set_goto_target( float3 new_goto_target, float distance_min, float distance_max = float.MaxValue )
		{
			update_goto_target( new_goto_target );

			commander.target_distance_range_min = distance_min;
			commander.target_distance_range_max = distance_max;
		}
		private
		void
		set_goto_target( float3 new_goto_target, float distance = 0 )
		{
			set_goto_target( new_goto_target, distance, distance );
		}
		private
		void
		set_escape_target( Knowledge_Base.Known_Entity ke, float min_range, float max_range )
		{
			commander.entity_target = null;
			set_interaction_target( ke, Interaction.NONE,
			                        min_range,
			                        max_range );
		}
		private
		void
		set_fight_target( Knowledge_Base.Known_Entity ke )
		{
			commander.entity_target = ke.entity;
			set_interaction_target( ke, Interaction.ATTACK,
			                        entity.status.attack_min_range,
			                        entity.status.attack_max_range );
		}
		private
		void
		set_heal_target( Knowledge_Base.Known_Entity ke )
		{
			commander.entity_target = ke.entity;
			set_interaction_target( ke, Interaction.HEAL,
			                        entity.status.cast_min_range,
			                        entity.status.cast_max_range );
		}
		private
		void
		set_interaction_target( Knowledge_Base.Known_Entity ke, Interaction i, float min_range, float max_range )
		{
			float radii = ke.radius + entity.radius;
			set_goto_target( ke.position,
			                 radii + min_range,
			                 radii + max_range );
			interaction_on_target = i;
			action_target_entity = ke;
			target_creation_time = target_update_time = Time.time;
		}
		private
		void
		act()
		{
			if ( interaction_on_target == Interaction.ATTACK )
			{
				if ( weapon.has_cast && ! weapon.cast_is_heal )
				{
					commander.cast();
				} else
				{
					commander.attack();
				}
			} else
			if ( interaction_on_target == Interaction.HEAL )
			{
				if ( weapon.has_cast && weapon.cast_is_heal )
				{
					commander.cast();
				} else
				{
					// TODO(theGiallo): is this ok?
					commander.attack();
				}
			}
		}
		private
		bool
		look_for_target_entity()
		{
			bool ret = false;

			Knowledge_Base.Known_Entity ke_fight_target = null;
			Knowledge_Base.Known_Entity ke_help_target = null;
			bool help_is_heal = true;
			Knowledge_Base.Known_Entity ke_escape_target = null;
			float min_safe_dist = 0, max_safe_dist = float.MaxValue;
			{
				if ( weapon != null
				  && ( weapon.has_melee || ( weapon.has_cast && ! weapon.cast_is_heal ) )
				  && kb.most_hostile( out Knowledge_Base.Known_Entity hostile,
				                      out float                       hostile_distance,
				                      out Hostility_Per_Distance      hostility_per_distance,
				                      out int                         hostility_index,
				                      news_limit_secs,
				                      Knowledge_Base.Which_Hostility.GENERAL ) )
				{
					if ( should_fight( hostile ) )
					{
						ke_fight_target = hostile;//set_fight_target( hostile );
					}
				}
			}
			{
				if ( kb.most_hostile( out Knowledge_Base.Known_Entity hostile,
				                      out float                       hostile_distance,
				                      out Hostility_Per_Distance      hostility_per_distance,
				                      out int                         hostility_index,
				                      news_limit_secs,
				                      Knowledge_Base.Which_Hostility.AT_THIS_DISTANCE ) )
				{
					ke_escape_target = hostile;
					bool has_safe_zone =
					   hostility_per_distance.nearest_safe_zone( hostile_distance,
					                                             out int safe_index,
					                                             kb.hostility_is_friend );
					if ( has_safe_zone )
					{
						Hostility_Per_Distance.zone_range( safe_index, out min_safe_dist, out max_safe_dist );
					} else
					{
						Hostility_Per_Distance.zone_range( Hostility_Per_Distance.BUCKETS_COUNT - 1,
						                                   out min_safe_dist,
						                                   out max_safe_dist );
					}
				}
			}
			{
				if ( weapon != null
				  && weapon.has_cast
				  && weapon.cast_is_heal
				  && kb.friend_in_need_of_heealing( out Knowledge_Base.Known_Entity friend, news_limit_secs ) )
				{
					ke_help_target = friend;
				} else
				if ( weapon != null
				  && ( weapon.has_melee || ( weapon.has_cast && ! weapon.cast_is_heal ) )
				  && false
				  // TODO(theGiallo): get an enemy that is attacking a friend
				)
				{
					help_is_heal = false;
				}
			}

			Array.Sort( actions_weights );
			for ( int i = 0; i != actions_weights.Length && ! ret; ++i )
			{
				Weighted_Action wa = actions_weights[i];
				switch ( wa.type )
				{
					case Action_Type.KILL:
						if ( ke_fight_target != null )
						{
							set_fight_target( ke_fight_target );
							action_type = wa.type;
							ret = true;
						}
						break;
					case Action_Type.HELP:
						if ( ke_help_target != null )
						{
							if ( help_is_heal )
							{
								set_heal_target( ke_help_target );
							} else
							{
								set_fight_target( ke_help_target );
							}
							action_type = wa.type;
							ret = true;
						}
						break;
					case Action_Type.SURVIVE:
						if ( ke_escape_target != null )
						{
							set_escape_target( ke_escape_target, min_safe_dist, max_safe_dist );
						}
						break;
				}
			}
			return ret;
		}
		private
		bool
		is_on_goto_target()
		{
			return commander.is_at_target;
		}
		private
		bool
		is_on_look_target()
		{
			return commander.is_looking_at_target;
		}
		private
		bool
		target_entity_is_no_more_target()
		{
			bool ret = false;
			Knowledge_Base.Known_Entity ke = action_target_entity;
			switch ( interaction_on_target )
			{
				case Interaction.ATTACK:
					ret = ! ke.is_alive;
					break;
				case Interaction.HEAL:
					ret = ! ke.is_alive || ke.hp < ke.max_hp;
					break;
				case Interaction.NONE:
					break;
			}
			return ret;
		}
		private
		void
		update_target_pos_rot()
		{
			update_goto_target( action_target_entity.position );
			set_look_target(  action_target_entity.position );
		}
		private
		float
		target_time_age_secs()
		{
			float ret = Time.time - target_creation_time;
			return ret;
		}
		private
		void
		set_roaming_target()
		{
			float3 p  = entity.pos;
			float3 fw = entity.fw;
			float dist = entity.radius + UnityEngine.Random.Range( roaming_min_dist, roaming_max_dist );
			float deg  = tg.fast_bell_random( roaming_rotation_mean_degs, roaming_rotation_radius );
			deg *= tg.random_sign();
			Quaternion rot = Quaternion.AngleAxis( deg, Vector3.up );
			float3 target = p + float3( rot * fw * dist );
			target_creation_time = target_update_time = Time.time;
			set_goto_target( target );
			set_look_target( target );
			commander.entity_target = null;
		}
		private static
		Initial_Hostility_Per_Distance[]
		make_initial_hostilities_per_dist_per_type()
		{
			Initial_Hostility_Per_Distance[] ret = new Initial_Hostility_Per_Distance[(int)Entity.Type.__COUNT];
			for ( int i = 0; i != ret.Length; ++i )
			{
				ret[i] = new Initial_Hostility_Per_Distance();
				ret[i].type = (Entity.Type)i;
				ret[i].hostility_per_distance = new Hostility_Per_Distance();
			}
			return ret;
		}
		#endregion

		#region Unity
		private void Awake()
		{
			update_timer = new Timer( update_period_secs , true, update_period_secs * ( UnityEngine.Random.value - 0.5f ) );

			kb.entity = entity;
			kb.initialize( initial_hostilities_per_dist_per_type );
			entity.status.weapon.on_will_change.Subscribe( WEAPON_CHANGE_PRIORITY, my_weapon_changed );
			kb.entity.status.hp.on_did_change.Subscribe( HP_CHECK_PRIORITY, my_hp_changed );
			kb.entity.status.is_alive.on_did_change.Subscribe( ALIVE_CHECK_PRIORITY, my_alive_changed );

			set_goto_target( entity.transform.position );

			look_layer_mask = ~LayerMask.GetMask( "step_checker" );
		}
		private void Start()
		{
			//kb.entity.status.normal_max_hp.on_did_change.Subscribe( 0,
			//( d ) =>
			//{
			//	kb.my_max_hp = entity.status.normal_max_hp;
			//	return Chained.CONTINUE;
			//} );
			//kb.my_max_hp = entity.status.normal_max_hp;

			commander.entity_radius = entity.radius;
		}
		private void Update()
		{
			if ( update_timer.loopback() )
			{
				kb.resolve_hostility_timestep( news_limit_secs );
				update_knowledge();

				if ( action_target_entity != null )
				{
					if ( action_target_entity.age_secs() > target_entity_expiring_secs )
					{
						if ( look_around.active )
						{
							if ( ! look_around.update( entity.pos, is_on_look_target(), set_look_target ) )
							{
								look_for_target_entity();
							}
						} else
						{
							look_around.activate( entity.rot );
						}
					} else
					if ( target_entity_is_no_more_target() )
					{
						look_for_target_entity();
					} else
					{
						if ( is_on_look_target() && is_on_goto_target() )
						{
							act();
						}
						update_target_pos_rot();
					}
				} else
				{
					if ( ! look_for_target_entity() )
					{
						if ( ( is_on_goto_target() && is_on_look_target() )
						  || target_time_age_secs() > roaming_expiring_secs )
						{
							set_roaming_target();
						}
					}
				}
			}
		}
		#endregion
	}
}
