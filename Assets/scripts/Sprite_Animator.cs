﻿using System;
using System.Collections;
using System.Collections.Generic;
using tg;
using UnityEngine;

namespace tg
{
	public class Sprite_Animator : MonoBehaviour
	{
		public Character_Controller character_controller;
		public Tile_Map_Sprite map_sprite;
		public float idle_duration_secs = 2f;
		public float meters_per_cycle = 0.7f;

		public float current_step_percentage { get; private set; }
		public float current_step_percentage_length { get; private set; }
		public int   current_sprite_animation_x => current_tile_x;

		public void change_character_tile()
		{
			starting_tile_x = map_sprite.tile_xy.x;
			current_tile_x = map_sprite.tile_xy.x;
		}

		#region private
		private enum
		Character_State
		{
			NONE,
			Idle,
			Walking,
		}

		private enum
		Animation_Frame
		{
			IdleStart = 0,
			IdleEnd = 1,
			MoveStart = 2,
			MoveRightStep = 3,
			MoveTransition = 4,
			MoveLeftStep = 5
		}

		private struct
		Animation_Boundaries
		{
			public Animation_Frame start;
			public Animation_Frame end;

			public Animation_Boundaries( Animation_Frame startState, Animation_Frame endState )
			{
				start = startState;
				end = endState;
			}
			public int steps_count => (int)end - (int)start + 1;
		}

		private Animation_Boundaries idle_boundaries = new Animation_Boundaries( Animation_Frame.IdleStart, Animation_Frame.IdleEnd );
		private Animation_Boundaries walk_boundaries = new Animation_Boundaries( Animation_Frame.MoveStart, Animation_Frame.MoveLeftStep );
		private int starting_tile_x;
		private int current_tile_x;
		private Animation_Frame current_animation_frame;
		private Character_State current_character_state;
		private Character_State previous_character_state;

		private
		void
		next_animation_step( Animation_Boundaries boundaries )
		{
			if ( current_animation_frame >= boundaries.end )
			{
				current_animation_frame = boundaries.start;
			} else
			{
				current_animation_frame = current_animation_frame + 1;
			}

			current_tile_x = starting_tile_x + (int)current_animation_frame;
		}

		private
		void
		set_animation( Animation_Boundaries ab, float delay_pc = 0 )
		{
			previous_character_state = current_character_state;
			current_tile_x = starting_tile_x + (int)ab.start;
			current_step_percentage = delay_pc;
			current_step_percentage_length = 1f / (float)ab.steps_count;
		}
		#endregion

		#region Unity
		void Start()
		{
			starting_tile_x = map_sprite.tile_xy.x;
			current_character_state = Character_State.Idle;
			current_animation_frame = Animation_Frame.IdleStart;
			set_animation( idle_boundaries, UnityEngine.Random.value / idle_boundaries.steps_count );
		}
		void Update()
		{
			current_character_state = character_controller.is_walking
			                        ? Character_State.Walking
			                        : Character_State.Idle;

			Animation_Boundaries ab = current_character_state == Character_State.Idle
			                       ? idle_boundaries : walk_boundaries;

			if ( current_character_state != previous_character_state )
			{
				set_animation( ab );
			} else
			{
				float dpc;
				if ( current_character_state == Character_State.Idle )
				{
					dpc = Time.deltaTime / idle_duration_secs;
				} else
				{
					float m = Time.deltaTime * character_controller.movement_speed;
					dpc = m / meters_per_cycle;
				}
				current_step_percentage += dpc;
				while ( current_step_percentage >= current_step_percentage_length )
				{
					current_step_percentage -= current_step_percentage_length;
					next_animation_step( ab );
				}
			}
		}
		#endregion
	}
}
