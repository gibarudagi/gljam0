﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Unity.Mathematics;
using static Unity.Mathematics.math;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;
using static tg.Character_Controller;
using System.IO;

namespace tg
{
	public class Player_Input_Commander : MonoBehaviour
	{

		[Serializable]
		public class
		Mapping : ISerializationCallbackReceiver
		{
			public const float MIN_SENSIBILITY = 0.0001f;
			public const float MAX_SENSIBILITY = 1000.0f;

			[SerializeField]
			public Game_Input.Mapping     mapping;
			[SerializeField]
			public Vector2 mouse_sensibility;

			// NOTE(theGiallo): indexed with Cartesian_Axis
			public bool[] inverted = new bool[2];

			public EventHandler on_change;

			[NonSerialized]
			public Vector2 mouse_delta;
			[NonSerialized]
			public Vector2 mouse_delta_sensible;

			void ISerializationCallbackReceiver.OnBeforeSerialize()
			{
			}
			void ISerializationCallbackReceiver.OnAfterDeserialize()
			{
				setup_mapping( on_deserialized: true );
			}

			public
			void
			copy_from( Mapping m )
			{
				mouse_sensibility   = m.mouse_sensibility;
				for ( int i = 0; i != inverted.Length; ++i )
				{
					inverted[i] = m.inverted[i];
				}
				mapping.copy_from( m.mapping );
			}

			public
			void
			setup_mapping()
			{
				setup_mapping( false );
			}
			private
			void
			setup_mapping( bool on_deserialized )
			{
				Game_Input.Input_Capture_Flags default_flags =
				     Game_Input.Input_Capture_Flags.DIGITIZED_WHEELS
				   | Game_Input.Input_Capture_Flags.INPUT_UNITY
				   | Game_Input.Input_Capture_Flags.NO_GAMEPADS;

				Game_Input.Mapping.Action[] actions = new Game_Input.Mapping.Action[ACTIONS_COUNT];
				for ( int i = 0; i != actions.Length; ++i )
				{
					actions[i] = new Game_Input.Mapping.Action();
					actions[i].flags = default_flags;
					actions[i].index = i;
				}
				bool ok = false;
				if ( on_deserialized )
				{
					ok = mapping.on_after_deserialized( actions );
				}
				if ( ! ok )
				{
					mapping = new Game_Input.Mapping( actions );
				}
			}
			public
			float
			get_sensibility( Cartesian_Axis axis )
			{
				float ret = mouse_sensibility[(int)axis];
				log( $"get sensibility {axis} => {ret}" );
				return ret;
			}
			public
			float
			set_sensibility( Cartesian_Axis axis, float v )
			{
				float ret = v;
				v = Mathf.Clamp( v, MIN_SENSIBILITY, MAX_SENSIBILITY );
				if ( mouse_sensibility[(int)axis] != v )
				{
					mouse_sensibility[(int)axis] = v;
					on_change?.Invoke( this, null );
				}
				return ret;
			}
			public
			bool
			set_inverted( Cartesian_Axis axis, bool inverted )
			{
				if ( this.inverted[(int)axis] != inverted )
				{
					this.inverted[(int)axis] = inverted;
					on_change?.Invoke( this, null );
				}
				return inverted;
			}
			public
			bool
			get_inverted( Cartesian_Axis axis )
			{
				return inverted[(int)axis];
			}


			public
			void
			remap_capture( Action_Index                    action,
			               Action<bool>                    callback,
			               Action<Action_Index,
			                      Game_Input.Input_Source,
			                      List<int>,
			                      Action<bool>>            confirmation_required )
			{
				var a = mapping.actions[(int)action];
				debug_assert( a.index == (int)action );
				tg_Manager.start_coroutine(
				Game_Input.instance.capture_input_source_co(
				   a.flags | Game_Input.Input_Capture_Flags.ABORT_ON_ESCAPE,
				   ( r ) => remap_capture_result_handler( action, r, callback, confirmation_required )
				) );
			}

			private
			void
			remap_capture_result_handler( Action_Index                    action,
			                              Opt<Game_Input.Input_Source>    result,
			                              Action<bool>                    callback,
			                              Action<Action_Index,
			                                     Game_Input.Input_Source,
			                                     List<int>,
			                                     Action<bool>>            confirmation_required )
			{
				bool did_change = false;
				if ( ! result.has_value )
				{
					callback?.Invoke( did_change );
					return;
				}
				if ( mapping.already_mapped( (int)action, result, out List<int> colliding_actions ) )
				{
					if ( confirmation_required != null )
					{
						confirmation_required.Invoke( action, result, colliding_actions,
							( bool confirmed )=>
							{
								if ( confirmed )
								{
									remap_removing_collisions( action, result, colliding_actions );
								}
								callback?.Invoke( confirmed );
							} );
					} else
					{
						remap_removing_collisions( action, result, colliding_actions );
						did_change = true;
						callback?.Invoke( did_change );
					}
				} else
				{
					remap( action, result );
					did_change = true;
					callback?.Invoke( did_change );
				}
			}

			private
			void
			remap( Action_Index action, Game_Input.Input_Source source )
			{
				mapping.sources[(int)action] = source;
				on_change?.Invoke( this, null );
			}

			private
			void
			remap_removing_collisions( Action_Index action, Game_Input.Input_Source source, List<int> colliding_actions )
			{
				mapping.remove_collisions( (int)action, source, colliding_actions );
				remap( action, source );
			}

			public
			void
			fixed_update()
			{
				mapping.fixed_update();
				mouse_delta = Mouse.current.delta.ReadValue();
				//log( $"{Time.frameCount:D4} mouse_delta = {mouse_delta}" );
				float2 inv = float2( get_inverted( Cartesian_Axis.HORIZONTAL ) ? -1f : 1f, get_inverted( Cartesian_Axis.VERTICAL ) ? 1f : -1f );
				mouse_delta = float2(mouse_delta) * inv;
				mouse_delta_sensible = mouse_delta * mouse_sensibility;
			}
		}

		[HideInInspector]
		public Mapping mapping;
		public EventHandler<Mapping> on_mapping_loaded;

		public Character_Controller controlled_character;

		#region private

		#region save_load_settings
		private const string mapping_file_name = "character_controller_mapping.json";
		private string default_mapping_file_path = Path.Combine( Application.streamingAssetsPath, "default_character_controller_mapping.json" );
		private string default_profile_name = "default";
		private
		bool
		load_mapping( byte[] data )
		{
			bool ret = false;
			string str = Encoding.UTF8.GetString( data );
			try {
				Mapping m = JsonUtility.FromJson<Mapping>( str );
				log( "loaded mapping" );
				log( m.get_sensibility( 0 ).ToString() );
				mapping.copy_from( m );
				on_mapping_loaded?.Invoke( this, mapping );
				ret = true;
			} catch ( Exception e )
			{
				log_err( e.Message );
			}
			return ret;
		}
		private
		void
		request_load_mapping()
		{
			string profile_name = default_profile_name;
			Save_Load_Manager.load( mapping_file_name,
			                        profile_name,
			( Opt<byte[]> data )=>
			{
				if ( data.has_value )
				{
					log( "got file" );
					bool did_load = load_mapping( data );
					if ( ! did_load )
					{
						log_err( "failed to load mappings" );
					}
				} else
				{
					Save_Load_Manager.load( default_mapping_file_path, null,
					( Opt<byte[]> def_data )=>
					{
						if ( def_data.has_value )
						{
							log( "got default mapping file" );
							bool did_load = load_mapping( def_data );
							if ( ! did_load )
							{
								log_err( "failed to load default mappings" );
							}
						} else
						{
							log( "didn't got any mapping file" );
						}
					}
					);
				}
			} );
		}
		private
		void
		request_save_mapping()
		{
			log( "request save" );
			string profile_name = default_profile_name;
			string s = JsonUtility.ToJson( mapping );
			byte[] data = Encoding.UTF8.GetBytes( s );
			Save_Load_Manager.save( mapping_file_name, profile_name, data );
		}
		#endregion save_load_settings


		private
		void
		update_control_commands( Controls_Commands controls_commands, Mapping mapping )
		{
			for ( int i = 0; i != ACTIONS_COUNT; ++i )
			{
				controls_commands.actions_on      [i] = action_on(       mapping, (Action_Index)i ) ? 1 : 0;
				controls_commands.actions_just_on [i] = action_just_on(  mapping, (Action_Index)i ) ? 1 : 0;
				controls_commands.actions_just_off[i] = action_just_off( mapping, (Action_Index)i ) ? 1 : 0;
			}
			controls_commands.delta_pitch_deg = delta_camera_pitch( mapping );
			controls_commands.delta_yaw_deg   = delta_camera_yaw( mapping );
		}
		private
		bool
		action_just_on( Mapping mapping, Action_Index a )
		{
			bool ret = mapping.mapping.actions_just_activated[(int)a] == 1;
			return ret;
		}
		private
		bool
		action_just_off( Mapping mapping, Action_Index a )
		{
			bool ret = mapping.mapping.actions_just_deactivated[(int)a] == 1;
			return ret;
		}
		private
		bool
		action_on( Mapping mapping, Action_Index a )
		{
			bool ret = mapping.mapping.actions_active[(int)a] == 1;
			return ret;
		}
		private
		float
		delta_camera_yaw( Mapping mapping )
		{
			float ret = mapping.mouse_delta_sensible.x;
			return ret;
		}
		private
		float
		delta_camera_pitch( Mapping mapping )
		{
			float ret = mapping.mouse_delta_sensible.y;
			return ret;
		}

		#endregion

		#region Unity
		private void Awake()
		{
			mapping = new Mapping();
			mapping.setup_mapping();
		}
		private void Start()
		{
			request_load_mapping();
			mapping.on_change += ( mapping, _ )=> request_save_mapping();
		}
		private void FixedUpdate()
		{
			mapping.fixed_update();
			if ( controlled_character != null )
			{
				update_control_commands( controlled_character.controls_commands, mapping );
			}
		}
		#endregion
	}
}
