﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace tg
{
	public interface IPooled<Self_Type> where Self_Type : IPooled<Self_Type>
	{
		void      put_back_in_pool();
		Self_Type new_one();
		int       get_pool_index();
		void      set_pool_index( int new_pool_index );
	}
	public class
	Pool<Pooled_Type> where Pooled_Type : IPooled<Pooled_Type>
	{
		public List<Pooled_Type> _pool;
		public int _count;
		public int _capacity;
		public Pooled_Type spawner;

		public
		void
		init( Pooled_Type spawner, int capacity = 16 )
		{
			this.spawner = spawner;
			_pool = new List<Pooled_Type>( capacity );
			_count = 0;
			_capacity = 0;
			grow( capacity );
		}

		public
		Pooled_Type
		take_away()
		{
			Pooled_Type ret;
			if ( is_full() )
			{
				grow();
			}
			ret = _pool[_count];
			++_count;
			return ret;
		}
		public
		void
		put_back( Pooled_Type e )
		{
			int e_index = e.get_pool_index();
			int last = _count - 1;
			Pooled_Type tmp = _pool[last   ];
			 _pool[last   ] = _pool[e_index];
			 _pool[e_index] = tmp;
			 _pool[last   ].set_pool_index( last    );
			 _pool[e_index].set_pool_index( e_index );
			--_count;
		}

		public
		bool
		is_full()
		{
			bool ret = _count == _capacity;
			return ret;
		}

		public
		void
		grow( int new_capacity = -1 )
		{
			new_capacity = new_capacity > _capacity ? new_capacity : _capacity * 2;
			_pool.Capacity = new_capacity;
			for ( int i = _capacity; i != new_capacity; ++i )
			{
				_pool.Add( spawner.new_one() );
				_pool[i].set_pool_index( i );
			}
			_capacity = new_capacity;
		}

		// NOTE(theGiallo): free_percentage is the size of the free part in percentage of the used one
		// e.g. _count = 100, _capacity = 150, free_count = 50, free_percentage = 100 / 50 = 0.5
		public
		void
		shrink_to( float free_percentage = 1.00f )
		{
			int target_capacity = Mathf.CeilToInt( _count * free_percentage );
			if ( target_capacity >= _capacity )
			{
				return;
			}
			_pool.RemoveRange( target_capacity, _capacity - target_capacity );
			_capacity = target_capacity;
		}
		public
		void
		shrink_to_fit()
		{
			shrink_to( 0 );
		}
	}
}
