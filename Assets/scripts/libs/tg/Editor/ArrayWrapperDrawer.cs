﻿using UnityEditor;
using UnityEngine;

// NOTE(theGiallo): you have to define a specific type and define the drawer type
// e.g.
// using MyType = Array_Wrapper<int>;
// [CustomPropertyDrawer( typeof( MyType ) )]
// public class MyTypeDrawer : ArrayWrapperDrawer<int> {}

[CustomPropertyDrawer( typeof( ArrayWrapper<> ), useForChildren: true )]
public
class
ArrayWrapperDrawer : PropertyDrawer
{
	private bool folded = true;

	public
	override
	float
	GetPropertyHeight( SerializedProperty property, GUIContent label )
	{
		//return EditorGUI.GetPropertyHeight( property, label, true );
		float ret = 0;

		GUIStyle style = EditorStyles.foldout;
		style.alignment = TextAnchor.UpperLeft;

		// Compute how large the button needs to be.
		Vector2 label_size = style.CalcSize( label );

		ret += label_size.y;

		SerializedProperty arrp = property.FindPropertyRelative( "array" );
		if ( arrp != null && !folded )
		{
			Debug.Assert( arrp.isArray );

			string tyep_str = arrp.arrayElementType;
			int size = arrp.arraySize;

			int old_indent = EditorGUI.indentLevel;
			++EditorGUI.indentLevel;

			for ( int i = 0; i != size; ++i )
			{
				SerializedProperty pe = arrp.GetArrayElementAtIndex( i );
				GUIContent l = new GUIContent( "Element " + i );
				float h = EditorGUI.GetPropertyHeight( pe, l );
				ret += h;
			}
		}
		return ret;
	}

	public
	override
	void
	OnGUI( Rect position, SerializedProperty property, GUIContent label )
	{
		//Debug.Log( "Array_Wrapper_Base_Drawer of " + property.displayName );

		SerializedProperty arrp = property.FindPropertyRelative( "array" );
		if ( arrp == null )
		{
			return;
		}
		Debug.Assert( arrp.isArray );

		string tyep_str = arrp.arrayElementType;
		int size = arrp.arraySize;

		EditorGUI.BeginProperty( position, label, property );

		// NOTE(theGiallo): drawing label
		GUIStyle style = EditorStyles.foldout;
		style.alignment = TextAnchor.UpperLeft;
		Vector2 label_size = style.CalcSize( label );
		Rect label_pos = new Rect( position.position, label_size );
		position.height -= label_size.y;
		position.y += label_size.y;
		folded = !EditorGUI.Foldout( label_pos, !folded, label, toggleOnLabelClick: true, style:style );

		if ( !folded )
		{
			int old_indent = EditorGUI.indentLevel;
			++EditorGUI.indentLevel;

			Rect rect = EditorGUI.IndentedRect( position );

			for ( int i = 0; i != size; ++i )
			{
				SerializedProperty pe = arrp.GetArrayElementAtIndex( i );
				string e_label = property.displayName + "[" + i + "] ";// + pe.displayName;
				SerializedProperty type_p = null;
				if ( ( type_p = pe.FindPropertyRelative( "type" ) ) != null )
				{
					e_label += type_p.enumDisplayNames[type_p.enumValueIndex];
				}
				GUIContent l = new GUIContent( e_label );
				EditorGUI.PropertyField( rect, pe, l, includeChildren: true );
				Vector2 pos = rect.position;
				float h = EditorGUI.GetPropertyHeight( pe, l );
				pos.y += h;
				rect.position = pos;
				rect.height -= h;
			}

			EditorGUI.indentLevel = old_indent;
		}

		EditorGUI.EndProperty();
	}
}