﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;
using UnityEngine.SceneManagement;

namespace tg
{
	public class MainMenu : MonoBehaviour
	{
		public string game_scene_name;

		public Button play_button;
		public Button exit_button;

		#region private
		private
		void
		play()
		{
			Loading.load( game_scene_name );
		}
		private
		void
		exit()
		{
			Application.Quit();
		}
		#endregion

		#region Unity
		private void Awake()
		{
			play_button.onClick.AddListener( play );
			exit_button.onClick.AddListener( exit );
		}
		private void Start()
		{
		}
		private void Update()
		{
		}
		#endregion
	}
}
