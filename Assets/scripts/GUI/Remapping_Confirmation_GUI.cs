﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;
using System;

namespace tg
{
	public class Remapping_Confirmation_GUI : MonoBehaviour
	{
		public Mapping_GUI mapping_gui;
		public TMP_Text source_text;
		public TMP_Text action_label_text;
		public TMP_Text colliding_actions_text;
		public Button   confirm_button;
		public Button   cancel_button;

		public
		void
		SetActive( bool active )
		{
			gameObject.SetActive( active );
		}

		#region private
		private
		void
		confirm_handler()
		{
			mapping_gui.confirmation_confirm();
		}
		private
		void
		cancel_handler()
		{
			mapping_gui.confirmation_cancel();
		}
		#endregion

		#region Unity
		private void Awake()
		{
		}
		private void Start()
		{
			confirm_button.onClick.AddListener( confirm_handler );
			cancel_button .onClick.AddListener( cancel_handler );
		}
		private void Update()
		{
		}
		#endregion
	}
}
