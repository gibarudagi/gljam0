﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;
using System;

namespace tg
{
	public class Remapping_Overlay_GUI : MonoBehaviour
	{
		public TMP_Text title_text;
		public TMP_Text action_label_text;

		public
		void
		SetActive( bool active )
		{
			gameObject.SetActive( active );
		}

		#region private
		#endregion

		#region Unity
		private void Awake()
		{
		}
		private void Start()
		{
		}
		private void Update()
		{
		}
		#endregion
	}
}
