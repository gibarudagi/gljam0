﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;
using System;

namespace tg
{
	public class Mouse_Entry_GUI : MonoBehaviour
	{
		public Mapping_GUI mapping_gui;
		public TMP_Text title_text;
		public Sensibility_Slider_GUI[] sensibility_slider_gui = new Sensibility_Slider_GUI[2];
		public Toggle horizontal_toggle;
		public Toggle vertical_toggle;

		public void set_mapping_gui( Mapping_GUI mapping_gui )
		{
			this.mapping_gui = mapping_gui;
			for ( int i = 0; i != sensibility_slider_gui.Length; ++i )
			{
				sensibility_slider_gui[i].set_mapping_gui( mapping_gui );
			}
			horizontal_toggle.onValueChanged.AddListener( ( bool on )=> mapping_gui.change_inverted( Cartesian_Axis.HORIZONTAL, on ) );
			vertical_toggle  .onValueChanged.AddListener( ( bool on )=> mapping_gui.change_inverted( Cartesian_Axis.VERTICAL,   on ) );

			horizontal_toggle.isOn = mapping_gui.mapping.get_inverted( Cartesian_Axis.HORIZONTAL );
			vertical_toggle  .isOn = mapping_gui.mapping.get_inverted( Cartesian_Axis.VERTICAL );
		}

		public
		void
		refresh()
		{
			refresh_in_update = false;
			title_text.text = "Mouse";
			vertical_layout?.SetLayoutVertical();
			horizontal_layout?.SetLayoutHorizontal();

			for ( int i = 0; i != sensibility_slider_gui.Length; ++i )
			{
				sensibility_slider_gui[i].refresh();
			}
			horizontal_toggle.isOn = mapping_gui.mapping.get_inverted( Cartesian_Axis.HORIZONTAL );
			vertical_toggle  .isOn = mapping_gui.mapping.get_inverted( Cartesian_Axis.VERTICAL );
		}

		#region private
		private VerticalLayoutGroup vertical_layout;
		private HorizontalLayoutGroup horizontal_layout;

		private bool refresh_in_update = false;

		#endregion

		#region Unity
		private void Awake()
		{
			horizontal_layout = transform.parent.GetComponent<HorizontalLayoutGroup>();
			vertical_layout = GetComponent<VerticalLayoutGroup>();

			for ( int i = 0; i != sensibility_slider_gui.Length; ++i )
			{
				sensibility_slider_gui[i].axis = (Cartesian_Axis)i;
			}
		}
		private void Start()
		{
		}
		private void Update()
		{
			if ( refresh_in_update )
			{
				refresh();
			}
		}
		#endregion
	}
}
