﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;
using UnityEngine.InputSystem;

namespace tg
{
	public class In_Game_Menu_Handler : MonoBehaviour
	{
		public static
		void
		unpause()
		{
			if ( backupped_time_scale >= 0 )
			{
				Time.timeScale = backupped_time_scale;
				backupped_time_scale = -1;
			} else
			if ( Time.timeScale == 0f )
			{
				Time.timeScale = 1f;
			}
		}
		public static
		void
		exit_menu()
		{
			instance.menu.SetActive( false );
			Character_Controller.set_in_game( true );

			unpause();
		}
		private static float backupped_time_scale = -1;
		public static
		void
		open_menu()
		{
			instance.menu.SetActive( true );
			Character_Controller.set_in_game( false );

			if ( backupped_time_scale < 0 )
			{
				backupped_time_scale = Time.timeScale;
			}
			Time.timeScale = 0;
		}

		public GameObject menu;

		#region private
		private static In_Game_Menu_Handler instance;
		#endregion

		#region Unity
		private void Awake()
		{
			instance = this;
			menu.SetActive( false );
		}
		private void Start()
		{
		}
		private void Update()
		{
			#if TG_RAW_INPUT
			bool got_esc =
			   Game_Input.instance.merged_keyboards_per_frame.key_front_down( Game_Input.Index.escape );
			#else
			//bool got_esc = Keyboard.current.escapeKey.wasPressedThisFrame;
			bool got_esc = Keyboard.current[Key.Escape].wasPressedThisFrame;
			#endif
			if ( got_esc )
			{
				if ( menu.activeSelf )
				{
					exit_menu();
				} else
				{
					open_menu();
				}
			}
		}
		#endregion
	}
}
