﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;
using System;

namespace tg
{
	public class Mapping_GUI : MonoBehaviour
	{
		public Player_Input_Commander.Mapping mapping;
		public Mapping_GUI_Entry              entry_prefab;
		public Transform                      entries_container;
		public Remapping_Overlay_GUI          remapping_overlay;
		public Remapping_Confirmation_GUI     confirmation_gui;
		public Mouse_Entry_GUI                mouse_entry;

		public
		string
		mapping_text( Character_Controller.Action_Index action_index )
		{
			string ret = source_text( mapping.mapping.sources[(int)action_index] );
			return ret;
		}
		public
		string
		source_text( Game_Input.Input_Source source )
		{
			string ret = source.ToString();
			if ( source.type == Game_Input.Input_Source.Type.KEY )
			{
				ret = source.key_code.ToString();
			} else
			if ( source.type == Game_Input.Input_Source.Type.MOUSE_BUTTON )
			{
				ret = source.mouse_button.ToString();
			}
			return ret;
		}

		public
		float
		change_sensibility( Cartesian_Axis axis, float v )
		{
			debug_assert( axis == Cartesian_Axis.HORIZONTAL || axis == Cartesian_Axis.VERTICAL );
			float ret = v;
			v = mapping.set_sensibility( axis, v );
			return ret;
		}
		public
		float
		get_sensibility( Cartesian_Axis axis )
		{
			debug_assert( axis == Cartesian_Axis.HORIZONTAL || axis == Cartesian_Axis.VERTICAL );
			float ret = mapping.get_sensibility( axis );
			return ret;
		}

		public
		void
		change_inverted( Cartesian_Axis axis, bool inverted )
		{
			debug_assert( axis == Cartesian_Axis.HORIZONTAL || axis == Cartesian_Axis.VERTICAL );
			if ( mapping.inverted[(int)axis] != inverted )
			{
				mapping.set_inverted( axis, inverted );
			}
		}


		public void confirmation_cancel()
		{
			confirmation_callback( false );
			confirmation_gui.SetActive( false );
			confirmation_callback = null;
		}

		public void confirmation_confirm()
		{
			confirmation_callback( true );
			confirmation_gui.SetActive( false );
			confirmation_callback = null;
		}

		public
		string
		action_text( Character_Controller.Action_Index action_index )
		{
			string ret = action_index.ToString();
			return ret;
		}
		public
		void
		request_remap( Character_Controller.Action_Index action_index )
		{
			remapping_action_index = action_index;
			remapping_overlay.action_label_text.text = action_text( action_index );
			remapping_overlay.SetActive( true );
			mapping.remap_capture( action_index, remap_completion_handler, remap_confirmation_handler );
		}

		#region private
		private Character_Controller.Action_Index remapping_action_index;
		private Action<bool>               confirmation_callback;
		private List<int>                  colliding_actions;
		private Mapping_GUI_Entry[]        entries;

		private
		void
		remap_completion_handler( bool remapped )
		{
			remapping_overlay.SetActive( false );
			if ( remapped )
			{
				var e = entries[(int)remapping_action_index];
				e.refresh();
				if ( colliding_actions != null )
				{
					for ( int i = 0; i != colliding_actions.Count; ++i )
					{
						entries[colliding_actions[i]].refresh();
					}
				}
			}
			colliding_actions = null;
		}
		private
		void
		remap_confirmation_handler( Character_Controller.Action_Index action_index,
		                            Game_Input.Input_Source    source,
		                            List<int>                  colliding_actions,
		                            Action<bool>               confirmation_callback )
		{
			this.confirmation_callback = confirmation_callback;
			this.colliding_actions     = colliding_actions;
			confirmation_gui.action_label_text.text = action_text( action_index );
			confirmation_gui.source_text.text = source_text( source );
			string colliding_str = "";
			for ( int i = 0; i != colliding_actions.Count; ++i )
			{
				colliding_str += $"{action_text( (Character_Controller.Action_Index)colliding_actions[i] )}\n";
			}
			confirmation_gui.colliding_actions_text.text = colliding_str;
			confirmation_gui.SetActive( true );
		}

		private
		void
		refresh()
		{
			for ( int i = 0, ic = entries.Length; i != ic; ++i )
			{
				entries[i].refresh();
			}
			mouse_entry.refresh();
		}
		#endregion

		#region Unity
		private void Awake()
		{
			for ( int s_i = 0; s_i != mouse_entry.sensibility_slider_gui.Length; ++s_i )
			{
				mouse_entry.sensibility_slider_gui[s_i].min_value = Player_Input_Commander.Mapping.MIN_SENSIBILITY;
				mouse_entry.sensibility_slider_gui[s_i].max_value = Player_Input_Commander.Mapping.MAX_SENSIBILITY;
			}
		}
		private void Start()
		{
			confirmation_gui.mapping_gui = this;

			Player_Input_Commander player_input_controller = FindObjectOfType<Player_Input_Commander>();
			mapping = player_input_controller.mapping;
			player_input_controller.on_mapping_loaded += ( fpc, m )=>
			{
				mapping = m;
				refresh();
			};

			entries = new Mapping_GUI_Entry[Character_Controller.ACTIONS_COUNT];
			for ( int i = 0, ic = entries.Length; i != ic; ++i )
			{
				Character_Controller.Action_Index action_index = (Character_Controller.Action_Index)i;
				Mapping_GUI_Entry entry = Instantiate( entry_prefab, entries_container );
				entry.mapping_gui = this;
				entry.set_action_index( action_index );
				entries[i] = entry;
			}
			mouse_entry.set_mapping_gui( this );
			mouse_entry.refresh();

			confirmation_gui             .gameObject.SetActive( false );
			remapping_overlay            .gameObject.SetActive( false );
		}
		private void Update()
		{
		}
		#endregion
	}
}
