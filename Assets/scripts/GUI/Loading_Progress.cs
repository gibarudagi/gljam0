﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;

namespace tg
{
	public class Loading_Progress : MonoBehaviour
	{
		public TMP_Text progress_text;

		#region private
		#endregion

		#region Unity
		private void Awake()
		{
		}
		private void Start()
		{
		}
		private void Update()
		{
			progress_text.text = $"{Loading.progress*100f:000.00}%";
		}
		#endregion
	}
}
