﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;

namespace tg
{
	public class Mapping_Menu : MonoBehaviour
	{
		public Button back_button;
		public GameObject paused_menu;

		#region private
		private
		void
		back()
		{
			paused_menu.SetActive( true );
			gameObject.SetActive( false );
		}
		#endregion

		#region Unity
		private void Awake()
		{
			back_button.onClick.AddListener( back );
		}
		private void Start()
		{
		}
		private void Update()
		{
		}
		#endregion
	}
}
