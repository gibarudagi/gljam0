﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;
using UnityEngine.SceneManagement;
using System;

namespace tg
{
	public class Loading : MonoBehaviour
	{
		public static float progress        => instance?._progress ?? 0f;
		public static float progress_unload => instance?._progress_unload ?? 0f;
		public static float progress_load   => instance?._progress_load ?? 0f;

		public float _progress        => ( _progress_unload + _progress_load ) * 0.5f;
		public float _progress_unload => async_operation_unload?.progress ?? 0;
		public float _progress_load   => async_operation_load?.progress ?? 0;

		public static
		void
		load( string scene_name )
		{
			instance._load( scene_name );
		}

		public static
		void
		reload()
		{
			instance._load( SceneManager.GetActiveScene().name );
		}

		#region private
		private AsyncOperation async_operation_unload;
		private AsyncOperation async_operation_load;
		private Scene old_scene;
		private bool is_loading;

		private static Loading instance;

		private const string empty_scene_name = "EmptyScene";

		private
		void
		_load( string scene_name )
		{
			if ( is_loading )
			{
				return;
			}
			is_loading = true;

			gameObject.SetActive( true );

			old_scene = SceneManager.GetActiveScene();
			int old_scene_index = old_scene.buildIndex;

			#if false
			SceneManager.LoadSceneAsync( empty_scene_name, LoadSceneMode.Additive ).completed +=
			( AsyncOperation empty_o ) =>
			{
				if ( empty_o.isDone )
				{
					SceneManager.SetActiveScene( SceneManager.GetSceneByName( empty_scene_name ) );
					async_operation_unload = SceneManager.UnloadSceneAsync( old_scene_index );
					async_operation_unload.completed +=
					( AsyncOperation o )=>
					{
						if ( o.isDone )
						{
							async_operation_load = SceneManager.LoadSceneAsync( scene_name, LoadSceneMode.Additive );
							async_operation_load.allowSceneActivation = false;
							async_operation_load.completed +=
							( AsyncOperation uo )=>
							{
								if ( uo.isDone )
								{
									bool success = SceneManager.SetActiveScene( SceneManager.GetSceneByName( scene_name ) );
									debug_assert( success );
									is_loading = false;
									gameObject.SetActive( false );
									SceneManager.UnloadSceneAsync( empty_scene_name );
								}
							};
						}
					};
				}
			};
			#else
			async_operation_unload = SceneManager.LoadSceneAsync( empty_scene_name );
			async_operation_unload.completed +=
			( AsyncOperation empty_o ) =>
			{
				if ( empty_o.isDone )
				{
					async_operation_load = SceneManager.LoadSceneAsync( scene_name );
					//async_operation_load.allowSceneActivation = false;
					async_operation_load.completed +=
					( AsyncOperation uo )=>
					{
						if ( uo.isDone )
						{
							//bool success = SceneManager.SetActiveScene( SceneManager.GetSceneByName( scene_name ) );
							//debug_assert( success );
							is_loading = false;
							gameObject.SetActive( false );
							try {
							SceneManager.UnloadSceneAsync( empty_scene_name );
							} catch ( ArgumentException )
							{
							}
						}
					};
				}
			};
			#endif
		}
		#endregion

		#region Unity
		private void Awake()
		{
			if ( instance != null )
			{
				Destroy( gameObject );
				return;
			}
			debug_assert( instance == null );
			instance = this;
			gameObject.SetActive( false );
			DontDestroyOnLoad( gameObject );
		}
		private void Start()
		{
		}
		private void Update()
		{
		}
		#endregion
	}
}
