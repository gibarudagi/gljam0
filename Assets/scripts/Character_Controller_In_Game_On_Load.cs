﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;

namespace tg
{
	public class Character_Controller_In_Game_On_Load : MonoBehaviour
	{
		#region private
		#endregion

		#region Unity
		private void Awake()
		{
		}
		private void Start()
		{
			Character_Controller.set_in_game( true );
		}
		private void Update()
		{
		}
		private void OnDestroy()
		{
			Character_Controller.set_in_game( false );
		}
		#endregion
	}
}
