﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyButtons;
using Pixeye.Unity;
using static tg.tg;
using System;

namespace tg
{
	[RequireComponent(typeof(Camera))]
	public class Camera_Render_Events : MonoBehaviour
	{
		public enum Event
		{
			OnPreCull,
			OnPreRender,
			OnPostRender,
			//---
			__COUNT,
		}
		public static
		int
		Subscribe( ushort priority, Action<Camera> callback, Event e )
		{
			return events[(int)e].Subscribe( priority, callback );
		}
		public static
		bool
		Unsubscribe( int priority_i, Event e )
		{
			return events[(int)e].Unsubscribe( priority_i );
		}
		public static
		void
		Unsubscribe( Action<Camera> callback, Event e )
		{
			events[(int)e].Unsubscribe( callback );
		}

		#region private
		private static Event_With_Priority<Camera>[] events = new Event_With_Priority<Camera>[(int)Event.__COUNT];
		static Camera_Render_Events()
		{
			for ( int i = 0; i != events.Length; ++i)
			{
				events[i] = new Event_With_Priority<Camera>();
			}
		}
		private Camera my_camera;
		private
		void
		invoke_event( Event e )
		{
			events[(int)e].Invoke( my_camera );
		}
		#endregion

		#region Unity
		private void Awake()
		{
			my_camera = GetComponent<Camera>();
			debug_assert( my_camera != null );
		}
		private void OnPreCull()
		{
			invoke_event( Event.OnPreCull );
		}
		private void OnPreRender()
		{
			invoke_event( Event.OnPreRender );
		}
		private void OnPostRender()
		{
			invoke_event( Event.OnPostRender );
		}
		#endregion
	}
}
